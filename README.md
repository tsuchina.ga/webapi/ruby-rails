# Ruby on Rails

これまでRubyを触る機会がなかったのでこれを機に触ってみる。


## 環境開発

0. Docker Container作成

  ```sh
  > docker pull ruby
  > docker run -d -it --name ruby-rails -p 3000:3000 -v E:\Projects\tsuchinaga\webapi\ruby-rails:/root -w /root ruby
  ```

0. 初期設定

  ```sh
  > docker exec -it ruby-rails bash
  $ apt update -y
  $ apt install -y nodejs

  $ ruby -v
  ruby 2.5.0p0 (2017-12-25 revision 61468) [x86_64-linux]
  $ bundle --version
  Bundler version 1.16.1
  $ gem --version
  2.7.6

  $ bundle init
  $ gem install rails
  $ bundle install # 別にいらないけど

  $ rails --version
  Rails 5.1.5

  $ rails new webapi -d postgresql
  $ cd webapi
  $ bundle install
  $ rails server
  ```

  [localhost:3000](http://localhost:3000)に接続できればOK
